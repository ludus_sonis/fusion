/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>

//We won't test equalities on floating point values
#define SCAL int
#include "../src/algebra.h"

int main(void)
{
  int ret = 0;

  {
    scal v[3] = {0,0,0};
    scal vexpect[3] = {1,1,1};
    alg_vss(3,v,1.0);
    if (0 != memcmp(v, vexpect, sizeof(v))) {
      perror("alg_vss test FAILED");
      ret = 1;
    }
  }
  { 
    scal v[3] = {1,1,1};
    scal vexpect[3] = {2,2,2};
    alg_vsm(3,v,2);
    if (0 != memcmp(v, vexpect, sizeof(v))) {
      perror("alg_vsm test FAILED");
      ret = 1;
    }
  }

  {
    scal v0[3] = {1,1,1};
    scal v1[3] = {1,1,1};
    scal vexpect[3] = {2,2,2};
    alg_vvs(3,v0,v1);
    if (0 != memcmp(v0,vexpect,sizeof(v0)))
    {
      perror("alg_vvs test FAILED");
      ret = 1;
    }
  }
  {
    scal v0[3] = {1,2,3};
    scal v1[3] = {4,5,6};
    scal sexpect = 32;
    scal sres = alg_vvm(3,v0,v1);
    if (sexpect != sres)
    {
      perror("alg_vvm test FAILED");
      ret = 1;
    }
  }
  {
    scal v0[3] = {1,2,3};
    scal v1[3] = {4,5,6};
    scal vres[3];
    scal vexpect[3] = {-3,6,-3};
    alg_cross(v0,v1,vres);
    if (0 != memcmp(vres, vexpect, sizeof(vres)))
    {
      perror("alg_cross test FAILED");
      ret = 1;
    }
  }

  {
    scal m[2][2] = {{0,0},{0,0}};
    printf("%p\n", m[0]);
    scal s = 1;
    scal mexpect[2][2] = {{1,1},{1,1}};
    alg_mss(2,m,s);
    if (0 != memcmp(m,mexpect,sizeof(m)))
    {
      perror("alg_mss test FAILED");
      ret = 1;
    }
  }
  {
    scal m[2][2] = {{1,2},{3,4}};
    scal s = 2.0;
    scal mexpect[2][2] = {{2,4},{6,8}};
    alg_msm(2,m,s);
    if (0 != memcmp(m,mexpect,sizeof(m)))
    {
      perror("alg_msm test FAILED");
      ret = 1;
    }
  }

  {
    scal m[2][2] = {{1,2},{3,4}};
    scal v[2] = {5,6};
    scal vres[2];
    scal vexpect[2] = {17,39};
    alg_mvm(2,m,v,vres);
    if (0 != memcmp(vres,vexpect,sizeof(vres)))
    {
      perror("alg_mvm test FAILED");
      ret = 1;
    }
  }

  {
    scal m0[2][2] = {{1,2},{3,4}};
    scal m1[2][2] = {{5,6},{7,8}};
    scal mres[2][2];
    scal mexpect[2][2] = {{19,22},{43,50}};
    alg_mmm(2,m0,m1,mres);
    if (0 != memcmp(mres,mexpect,sizeof(mres)))
    {
      perror("alg_mmm test FAILED");
      ret = 1;
    }
  }

  {
    scal m0[2][2] = {{1,2},{3,4}};
    scal m1[2][2] = {{5,6},{7,8}};
    scal mexpect[2][2] = {{6,8},{10,12}};
    alg_mms(2,m0,m1);
    if (0 != memcmp(m0,mexpect,sizeof(m0)))
    {
      perror("alg_mms test FAILED");
      ret = 1;
    }
  }

  {
    scal m1[2][2] = {{5,6},{7,8}};
    scal v0[2] = {1,2};
    scal v1[2] = {3,4};
    scal vexpect[2] = {40,55};
    alg_v_mvm_s(2,v0,m1,v1);
    if (0 != memcmp(v0,vexpect,sizeof(v0)))
    {
      perror("alg_mvm_mvm_s test FAILED");
      ret = 1;
    }
  }

  {
    scal A[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
    scal tA[3][3];
    scal tAexpect[3][3] = {{1,4,7},{2,5,8},{3,6,9}};
    alg_trans(3,A,tA);
    if (0 != memcmp(tA,tAexpect,sizeof(tA)))
    {
      perror("alg_trans test FAILED");
      ret = 1;
    }
  }

  return ret;
}
