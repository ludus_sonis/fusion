/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SENSOR_H
#define SENSOR_H

#define ACCEL 0
#define GYRO 1
#define MAGN 2

#define MAX_SECONDS 0xff

typedef struct {
  float x, y, z, t;
} SensorData;

void do_open(int index, char const* dev);
void do_epoll_ctl(int index);
void do_input(int index, SensorData * data);
void initSensors(int argc, char const *argv[]);
int do_epoll_wait(struct epoll_event *evlist, int maxevents);
void closeSensors(void);

#endif
