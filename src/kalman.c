/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 * Some notations from "An Introduction to the Kalman Filter",
 * Greg Welch and Gary Bishop
 * Some from "A Fusion Method for Combining Low-Cost IMU/Magnetometer
 * Outputs for Use in Applications on Mobile Devices"
 * doi:10.3390/s18082616
 * 
 * In "A Fusion..." R is the rotation matrix or Direct Cosine Matrix,
 * the state. But R is also the letter used for the measurement noise
 * covariance matrix. Here the Direct Cosine Matrix is named DCM.
 */
#include <string.h>

#include "kalman.h"
#include "info.h"

/*
 * Projection to the horizontal plane of magnetometer readings
 * Also computes covariance using propagation covariance law in R
 * using covariances of magnetometers (V)
 */
void
computeHMandRProcess1(scal const DCM[3][3], scal const M[3], scal HM[2],
    scal const V[3], scal R[2], size_t mva)
{
  scal roll = rollFromDCM(DCM, mva);
  scal pitch = pitchFromDCM(DCM, mva);
  scal cosr = cos(roll), sinr = sin(roll);
  scal cosp = cos(pitch), sinp = sin(pitch);
  int indx = mvaToInd(mva,0);
  int indy = mvaToInd(mva,1);
  int indz = mvaToInd(mva,2);
  HM[0] = M[indx]*cosp + M[indy]*sinr*sinp + M[indz]*cosr*sinp;
  HM[1] = -(M[indy]*cosr - M[indz]*sinr);
  R[0] = pow2(cosp)*V[indx] + pow2(sinr*sinp)*V[indy] + pow2(cosr*sinp)*V[indz];
  R[1] = pow2(cosr)*V[indy] + pow2(sinr)*V[indz];
}

/*
 * equation is x = Ax + Bu but A is identity so x + Bu
 */
void
predictState(size_t n, scal x[n], scal B[n][n], scal u[n])
{
  alg_v_mvm_s(n, x, B, u);
}

/*
 *  equation is P = APtA + Q but A is identity, Q is diagonal
 * then P+Q is and we only store diagonal, we treat P and Q as vectors
 */
void
predictError(size_t n, scal P[n], scal Q[n])
{
  alg_vvs(n, P, Q);
}

/*
 * equation is K = PtH(HPtH + R)^-1 but H is identity so
 * P(P+R)^-1 and _a_priori_ P+R is diagonal each coef > 0
 * we only store diagonal
 */
void
computeGain(size_t n, scal P[n], scal R[n], scal K[n])
{
  for(size_t u=0; u<n; ++u) {
    K[u] = P[u]*(1.0/(P[u] + R[u]));
  }
}

/*
 * equation is x = x + K(z - Hx) but H is identity so
 * x + K(z - x). K diagonal
 */
void
correctState(size_t n, scal x[n], scal K[n], scal z[n])
{
  for(size_t u=0; u<n; ++u) {
    x[u] = x[u] + K[u]*(z[u]-x[u]);
  }
}

/*
 * equation is P = (I - KH)P, again H identity so
 * (I - K)P, K and P diag
 */
void
correctError(size_t n, scal K[n], scal P[n])
{
  for(size_t u=0; u<n; ++u) {
    P[u] = P[u] - K[u]*P[u];
  }
}

/*
 * result in Q
 * update process noise covariance, V is sampling variances
 * Q is diagonal, we treat as vector
 * B is antisymmetric, diag is 0, we use these to speed up computations
 */
void
computeQprocess0(scal B[3][3], scal V[3], scal Q[3])
{
  //pow2(B[i][j]) == pow2(B[j,i])
  scal pow2_B[3] = {pow2(B[0][1]),pow2(B[0][2]),pow2(B[1][2])};
  Q[0] = pow2_B[0]*V[1] + pow2_B[1]*V[2];
  Q[1] = pow2_B[0]*V[0] + pow2_B[2]*V[2];
  Q[2] = pow2_B[1]*V[0] + pow2_B[2]*V[1];
}

void
computeQprocess1(scal B[2][2], scal V[2], scal Q[2])
{
  Q[0] = pow2(B[0][0])*V[0] + pow2(B[0][1])*V[1];
  Q[1] = pow2(B[1][0])*V[0] + pow2(B[1][1])*V[1];
}

void
orthonormalizePredict(scal DCM[3][3])
{
  scal errord2 = alg_vvm(3, DCM[0], DCM[1])/2.0;
  scal Xortho[3], Yortho[3];
  for(size_t u=0; u<3; ++u)
  {
    Xortho[u] = DCM[0][u] - errord2*DCM[1][u];
    Yortho[u] = DCM[1][u] - errord2*DCM[0][u];
  }
  memcpy(DCM[0], Xortho, sizeof(Xortho));
  memcpy(DCM[1], Yortho, sizeof(Yortho));
  alg_cross(DCM[0], DCM[1], DCM[2]);
  for (size_t u=0; u<3; ++u) {
    alg_vsm(3,DCM[u],1.0/alg_magn(3,DCM[u]));
  }
}

void
DCMfromRollPitchYaw(scal DCM[3][3], size_t mva, scal roll, scal pitch, scal yaw)
{
  scal cosr=cos(roll), sinr=sin(roll);
  scal cosp=cos(pitch), sinp=sin(pitch);
  scal cosy=cos(yaw), siny=sin(yaw);
  size_t ind=mvaToInd(mva,0);
  DCM[0][ind]=cosp*cosy;
  DCM[1][ind]=cosp*siny;
  DCM[2][ind]=-sinp;
  ind=mvaToInd(mva,1);
  DCM[0][ind]=sinr*sinp*cosy - cosr*siny;
  DCM[1][ind]=sinr*sinp*siny + cosr*cosy;
  DCM[2][ind]=sinr*cosp;
  ind=mvaToInd(mva,2);
  DCM[0][ind]=cosr*sinp*cosy + sinr*siny;
  DCM[1][ind]=cosr*sinp*siny - sinr*cosy;
  DCM[2][ind]=cosr*cosp;

}

void
orthonormalizeCorrect(scal DCM[3][3], size_t mva)
{
  scal roll=rollFromDCM(DCM, mva), pitch=pitchFromDCM(DCM, mva),
       yaw=yawFromDCM(DCM, mva);
  DCMfromRollPitchYaw(DCM, mva, roll, pitch, yaw);
}

void
gyroInput(scal G[3], scal DCM[3][3], scal dt, scal V[3], scal Pp0[3], scal Pp1[2])
{
  //printf("G: %f %f %f\n", G[0], G[1], G[2]);
  //process 0 predict
  scal xp0[3];
  {
    memcpy(xp0, DCM[2], sizeof(xp0));
    scal DCM33dt = DCM[2][2]*dt, DCM32dt = DCM[2][1]*dt, DCM31dt = DCM[2][0]*dt;
    scal B[3][3] = {
      {0.0, -DCM33dt, DCM32dt},
      {DCM33dt, 0.0, -DCM31dt},
      {-DCM32dt, DCM31dt, 0.0}
    };
    predictState(3, xp0, B, G);
    scal Q[3];
    computeQprocess0(B, V, Q);
    predictError(3,Pp0,Q);
  }

  //process 1 predict
  scal xp1[2];
  size_t indx;
  {
    int mva = mostVerticalAxis(DCM);
    scal B[2][2];
    scal subG[2];
    scal subV[2];
    indx = mvaToInd(mva,0);
    switch (mva)
    {
      case 0:
        xp1[0]=DCM[0][indx];
        xp1[1]=DCM[1][indx];
        B[0][0]=DCM[0][2]*dt;
        B[0][1]=-DCM[0][0]*dt;
        B[1][0]=DCM[1][2]*dt;
        B[1][1]=-DCM[1][0]*dt;
        subG[0]=G[0];
        subG[1]=G[2];
        subV[0]=V[0];
        subV[1]=V[2];
        break;
      case 1:
        xp1[0]=DCM[0][indx];
        xp1[1]=DCM[1][indx];
        B[0][0]=-DCM[0][1]*dt;
        B[0][1]=DCM[0][0]*dt;
        B[1][0]=-DCM[1][1]*dt;
        B[1][1]=DCM[1][0]*dt;
        subG[0]=G[0];
        subG[1]=G[1];
        subV[0]=V[0];
        subV[1]=V[1];
        break;
      case 2:
        xp1[0]=DCM[0][indx];
        xp1[1]=DCM[1][indx];
        B[0][0]=-DCM[0][2]*dt;
        B[0][1]=DCM[0][1]*dt;
        B[1][0]=-DCM[1][2]*dt;
        B[1][1]=DCM[1][1]*dt;
        subG[0]=G[1];
        subG[1]=G[2];
        subV[0]=V[1];
        subV[1]=V[2];
        break;
    }
    predictState(2, xp1, B, subG);
    scal Q[2];
    computeQprocess1(B, subV, Q);
    predictError(2, Pp1, Q);
  }

  //merge results, orthonormalize
  memcpy(DCM[2], xp0, sizeof(xp0));
  DCM[0][indx] = xp1[0];
  DCM[1][indx] = xp1[1];
  orthonormalizePredict(DCM);
}

//A is accelerometer data
void
accelInput(scal A[3],scal DCM[3][3], scal dt, scal V[3], scal P[3])
{
  alg_norm(3,A);
  scal K[3];
  //variance is R here.
  computeGain(3, P, V, K);
  correctState(3, DCM[2], K, A);
  orthonormalizeCorrect(DCM, mostVerticalAxis(DCM));
  correctError(3, K, P);
}

void
magnInput(scal M[3], scal DCM[3][3], scal dt, scal V[3], scal P[2])
{
  alg_norm(3,M);
  scal K[2];
  scal HM[2];
  scal R[2];
  int mva = mostVerticalAxis(DCM);
  size_t indx = mvaToInd(mva, 0);;
  scal x[2] = { DCM[0][indx], DCM[1][indx] };
  computeHMandRProcess1(DCM, M, HM, V, R, mva);
  computeGain(2, P, R, K);
  correctState(2, x, K, HM);
  //integrate x into DCM
  DCM[0][indx] = x[0];
  DCM[1][indx] = x[1];
  orthonormalizeCorrect(DCM,mva);
  correctError(2, K, P);
}
