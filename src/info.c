/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "info.h"

void
printVector(size_t n, scal V[n])
{
  fprintf(stderr, "[ ");
  for(size_t u=0; u<n; ++u) {
    fprintf(stderr, "%f ", V[u]);
  }
  fprintf(stderr, "]\n");
}

void
printMatrix(size_t n, scal M[n][n])
{
  fprintf(stderr, "{\n");
  for(size_t u0=0; u0<n; ++u0)
  {
    for(size_t u1=0; u1<n; ++u1)
      fprintf(stderr, "%f ", M[u0][u1]);
    fprintf(stderr, "\n");
  }
  fprintf(stderr, "}\n");
}


