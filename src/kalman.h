/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef KALMAN_H
#define KALMAN_H

#include "algebra.h"
//
//MAYBEDONE: replace fabs with generic macro...

size_t
INLINE mostVerticalAxis(scal DCM[3][3])
{
	scal* z = DCM[2];
	size_t mva = fabs(z[0]) > fabs(z[1]) ? 0 : 1;
	mva = fabs(z[mva]) > fabs(z[2]) ? mva : 2;
	return mva;
}

/*
 * Indirection, what is column of vector ind when most vertical axis
 * is mva?
 */
size_t
INLINE mvaToInd(size_t mva, size_t ind)
{
  return (mva + ind + 1) % 3;
}

//yaw or heading, psi
INLINE scal
yawFromDCM(scal const DCM[3][3], size_t mva)
{
  size_t indx = mvaToInd(mva,0);
  return atan2(DCM[1][indx], DCM[0][indx]);
}

//theta
INLINE scal
pitchFromDCM(scal const DCM[3][3], size_t mva)
{
  size_t indx = mvaToInd(mva,0), indy = mvaToInd(mva, 1),
         indz = mvaToInd(mva,2);
  return atan2(-DCM[2][indx], sqrt(pow2(DCM[2][indy]) + pow2(DCM[2][indz])));
}

//phi
INLINE scal
rollFromDCM(scal const DCM[3][3], size_t mva)
{
  size_t indy = mvaToInd(mva, 1), indz = mvaToInd(mva,2);
  return atan2(DCM[2][indy], DCM[2][indz]);
}


void DCMfromRollPitchYaw(scal DCM[3][3], size_t mva, scal roll, scal pitch,
    scal yaw);
void gyroInput(scal G[3], scal DCM[3][3], scal dt, scal V[3], scal Pp0[3],
    scal Pp1[2]);
void accelInput(scal A[3],scal DCM[3][3], scal dt, scal V[3], scal P[3]);
void magnInput(scal M[3], scal DCM[3][3], scal dt, scal V[3], scal P[2]);

#endif
