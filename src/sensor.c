/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sys/stat.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <inttypes.h>
#include <endian.h>

#include "error.h"
#include "sensor.h"

float g_aunits;
float g_gunits;
float g_munits;
float g_munitsz;

float g_abiasx = 0.0;
float g_abiasy = 0.0;
float g_abiasz = 0.0;
float g_ascalex = 1.0;
float g_ascaley = 1.0;
float g_ascalez = 1.0;

float g_gbiasx = 0.0;
float g_gbiasy = 0.0;
float g_gbiasz = 0.0;

float g_mbiasx = 0.0;
float g_mbiasy = 0.0;
float g_mbiasz = 0.0;
float g_mscalex = 1.0;
float g_mscaley = 1.0;
float g_mscalez = 1.0;


typedef struct {
  int16_t x __attribute__((aligned(2)));
  int16_t y __attribute__((aligned(2)));
  int16_t z __attribute__((aligned(2)));
  uint64_t t __attribute__((aligned(8)));
} __attribute__((packed)) RawSensorData;

int fd_epoll;
int fds[3];
FILE *streams[3];

// beware, that's non generic. This function must be written for each set of sensor.
// Here this is for lsm303dlhc + l3g4200d
void
rawToGeneric(RawSensorData const* raw, SensorData * generic, int type)
{
  switch (type) {
    case ACCEL:
      generic->x = ((((float)(((int16_t)le16toh(raw->x))>>4))*g_aunits)-g_abiasx)*g_ascalex;
      generic->y = ((((float)(((int16_t)le16toh(raw->y))>>4))*g_aunits)-g_abiasy)*g_ascaley;
      generic->z = ((((float)(((int16_t)le16toh(raw->z))>>4))*g_aunits)-g_abiasz)*g_ascalez;
      break;
    case GYRO:
      generic->x = ((float)(int16_t)le16toh(raw->x))*g_gunits-g_gbiasx;
      generic->y = ((float)(int16_t)le16toh(raw->y))*g_gunits-g_gbiasy;
      generic->z = ((float)(int16_t)le16toh(raw->z))*g_gunits-g_gbiasz;
      break;
    case MAGN:
      generic->x = ((((float)(int16_t)be16toh(raw->x))*g_munits)-g_mbiasx)*g_mscalex;
      generic->y = ((((float)(int16_t)be16toh(raw->y))*g_munits)-g_mbiasy)*g_mscaley;
      generic->z = ((((float)(int16_t)be16toh(raw->z))*g_munitsz)-g_mbiasz)*g_mscalez;
      break;
  }
      uint64_t t = le64toh(raw->t);
      generic->t = ((float)((t/1000000000)&MAX_SECONDS)) + (float)((t/1000)%1000000)*0.000001;
}

void
do_open(int index, char const* dev)
{
  int fd = open(dev, O_RDONLY | O_NONBLOCK);
  if (-1 == fd)
    DIE("open %s", dev);
  streams[index] = fdopen(fd, "r");
  if (NULL == streams[index])
    DIE_ERRNO("fdopen %s", dev);
  fds[index] = fd;
}

void
do_epoll_ctl(int index)
{
  struct epoll_event ev;
  int ret;
  int fd = fds[index];

  ev.events = EPOLLIN | EPOLLET;
  ev.data.fd = index;
  ret = epoll_ctl(fd_epoll, EPOLL_CTL_ADD, fd, &ev);
  if (-1 == ret)
    DIE_ERRNO("epoll_ctl");
}

void
do_input(int index, SensorData *data)
{
    FILE* stream = streams[index];
    RawSensorData rawdata;
    for (;;) {
      size_t n = fread(&rawdata, sizeof(rawdata), 1, stream);
      if (n != 1)
        return;
      rawToGeneric(&rawdata, data, index);
    }
}

void
initSensors(int argc, char const *argv[])
{
  g_aunits = strtof(argv[10], NULL);
  g_gunits = strtof(argv[11], NULL);
  g_munits = strtof(argv[12], NULL);
  g_munitsz = strtof(argv[13], NULL);

  if (29 == argc) {
    g_abiasx = strtof(argv[14], NULL);
    g_abiasy = strtof(argv[15], NULL);
    g_abiasz = strtof(argv[16], NULL);
    g_ascalex = strtof(argv[17], NULL);
    g_ascaley = strtof(argv[18], NULL);
    g_ascalez = strtof(argv[19], NULL);

    g_gbiasx = strtof(argv[20], NULL);
    g_gbiasy = strtof(argv[21], NULL);
    g_gbiasz = strtof(argv[22], NULL);

    g_mbiasx = strtof(argv[23], NULL);
    g_mbiasy = strtof(argv[24], NULL);
    g_mbiasz = strtof(argv[25], NULL);
    g_mscalex = strtof(argv[26], NULL);
    g_mscaley = strtof(argv[27], NULL);
    g_mscalez = strtof(argv[28], NULL);
  }

  fd_epoll = epoll_create1 (0);
  if (-1 == fd_epoll)
    DIE_ERRNO("epoll_create1");
  do_open (0, "/dev/accelerometer");
  do_epoll_ctl (0);
  do_open (1, "/dev/gyroscope");
  do_epoll_ctl (1);
  do_open (2, "/dev/magnetometer");
  do_epoll_ctl (2);
}

int
do_epoll_wait(struct epoll_event *evlist, int maxevents)
{
  int nfds = epoll_wait(fd_epoll, evlist, maxevents, -1);
  if (-1 == nfds)
    DIE_ERRNO("epoll_wait");
  return nfds;
}

void
closeSensors(void)
{
  for (int i = 0; i < 3; ++i)
    (void) fclose (streams[i]);
  (void) close (fd_epoll);
}
