/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sys/epoll.h>

#include "error.h"
#include "sensor.h"
#include "info.h"

#define MAX_EVENTS 10

int
main(int argc, char const* argv[])
{
  SensorData data;

  int nfds;
  struct epoll_event evlist[MAX_EVENTS];

  if (argc != 14 && argc != 29)
    DIE ("usage: %s avarx avary avarz "
      "gvarx gvary gvarz mvarx mvary mvarz "
      "aunits gunits munits munitsz [abiasx abiasy abiasz "
      "ascalex ascaley ascalez gbiasx gbiasy gbiasz " 
      "mbiasx mbiasy mbiasz "
      "mscalex mscaley mscalez]", argv[0]);

  initSensors(argc, argv);

  for (;;) {
      nfds = do_epoll_wait (evlist, MAX_EVENTS);

      for (int n = 0; n < nfds; ++n) {
	      int index = evlist[n].data.fd;
	      uint32_t events = evlist[n].events;

	      if (events & EPOLLIN) {
	        do_input (index, &data);

          printf("%f %f %f\n", data.x, data.y, data.z);
          fflush(stdout);
	      }
        else
          DIE("BUG");
	    }
  }
  
  closeSensors();

  exit(EXIT_SUCCESS);
}
