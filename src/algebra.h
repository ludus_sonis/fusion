/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ALGEBRA_H
#define ALGEBRA_H

#include <stddef.h>
#include <math.h>

//this is just for unit test with integral type..
#ifndef SCAL
#define SCAL float
#endif
typedef SCAL scal; //scalar

/* alg: algebra namespace, _v_s_s_: _vector_ to _scalar_ _sum_
 * all function named that way with types vector, matrix and scalar
 * and operations sum and mult.
 * operation that can be done inplace without needing other memory and copy, are
 * no bound check
 */

#define INLINE __attribute__((always_inline)) inline
#define for_(u,n) for(size_t (u)=0;(u)<(n);++(u))

#define SQRT(x) _Generic((x), float: sqrtf, default: sqrt)(x)

INLINE scal
pow2(scal s)
{
  return s*s;
}

//magnitude
INLINE scal
alg_magn(size_t n, scal v[n])
{
  scal m=0.0;
  for_(u,n) {
    m += pow2(v[u]);
  }
  return SQRT(m);
}


//vector - scalar: result in vector
#define alg_vs_(n,v,s,op) \
for_(u,(n)) { \
  (v)[u] op (s); \
}
INLINE void
alg_vss(size_t n, scal v[n], scal s)
{
  alg_vs_(n,v,s,+=);
}
INLINE void
alg_vsm(size_t n, scal v[n], scal s)
{
  alg_vs_(n,v,s,*=);
}

//normalize the vector
INLINE void
alg_norm(size_t n, scal v[n])
{
  alg_vsm(n, v, 1.0/alg_magn(n, v));
}

//vector - vector:
#define alg_vv_(n,v0,v1,op) \
for_(u,(n)) { \
  (v0)[u] op (v1)[u]; \
}
//sum each couple
INLINE void
alg_vvs(size_t n, scal v0[n], scal v1[n])
{
  for_(u,n) {
    v0[u] += v1[u];
  }
}
//return the dot product
INLINE scal
alg_vvm(size_t n, scal v0[n], scal v1[n])
{
  scal res = 0.0;
  for_(u,n) {
    res += v0[u] * v1[u];
  }
  return res;
}

//cross product
INLINE void
alg_cross(scal u[3], scal v[3], scal vres[3])
{
  vres[0] = u[1]*v[2] - u[2]*v[1];
  vres[1] = u[2]*v[0] - u[0]*v[2];
  vres[2] = u[0]*v[1] - u[1]*v[0];
}

//matrix - scalar: result in matrix
#define alg_ms_(n,m,s,op) \
for_(u1,(n)) { \
  alg_vs_((n),(m)[u1],(s),op); \
}
INLINE void
alg_mss(size_t n, scal m[n][n], scal s)
{
  alg_ms_(n,m,s,+=);
}
INLINE void
alg_msm(size_t n, scal m[n][n], scal s)
{
  alg_ms_(n,m,s,*=);
}

//matrix - vector: only mult relevant, must supply vres for result
INLINE void
alg_mvm(size_t n, scal m[n][n], scal v[n], scal vres[n])
{
  for_(u0,n) {
    vres[u0] = 0.0;
    for_(u1,n) {
      vres[u0] += m[u0][u1]*v[u1];
    }
  }
}

//matrix - matrix:
INLINE void
alg_mmm(size_t n, scal m0[n][n], scal m1[n][n], scal mres[n][n])
{
  for_(u0,n) {
    for_(u1,n) {
      mres[u0][u1] = 0.0;
      for_(u2,n) {
        mres[u0][u1] += m0[u0][u2]*m1[u2][u1];
      }
    }
  }
}

//result in m0
INLINE void
alg_mms(size_t n, scal m0[n][n], scal m1[n][n])
{
  for_(u0,n) {
    for_(u1,n) {
        m0[u0][u1] += m1[u0][u1];
    }
  }
}

//following for kalman, less loops, inplace in v0
//vector + matrix*vector
INLINE void
alg_v_mvm_s(size_t n, scal v0[n], scal m1[n][n], scal v1[n])
{
  for_(u0,n) {
    for_(u1,n) {
      v0[u0] += m1[u0][u1] * v1[u1];
    }
  }
}

//transpose A, result in tA
INLINE void
alg_trans(size_t n, scal A[n][n], scal tA[n][n])
{
  for_(u0,n) {
    for_(u1,n) {
      tA[u1][u0] = A[u0][u1];
    }
  }
}

#endif
