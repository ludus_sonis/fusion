/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sys/epoll.h>
#include <fcntl.h>
#include <unistd.h>

#include "error.h"
#include "kalman.h"
#include "sensor.h"
#include "info.h"

#define MAX_EVENTS 10

int
main(int argc, char const* argv[])
{
  int flags = fcntl(1, F_GETFL);
  flags = fcntl(1, F_SETFL, flags | O_NONBLOCK);
  if (-1 == flags)
    DIE_ERRNO("fcntl F_SETFL");

  scal DCM[3][3];
  DCMfromRollPitchYaw(DCM, 2, 0.0, 0.0, 0.0);

  scal Va[3];
  scal Vg[3];
  scal Vm[3];
  scal Pp0[3] = { 0, 0, 0 };
  scal Pp1[2] = { 0, 0 };
  scal lastts[3] = {0.0,0.0,0.0};
  SensorData data;

  int nfds;
  struct epoll_event evlist[MAX_EVENTS];

  if (argc != 14 && argc != 29)
    DIE ("usage: %s avarx avary avarz "
      "gvarx gvary gvarz mvarx mvary mvarz "
      "aunits gunits munits munitsz [abiasx abiasy abiasz "
      "ascalex ascaley ascalez gbiasx gbiasy gbiasz " 
      "mbiasx mbiasy mbiasz "
      "mscalex mscaley mscalez]", argv[0]);

  initSensors(argc, argv);
  Va[0] = strtof(argv[1], NULL);
  Va[1] = strtof(argv[2], NULL);
  Va[2] = strtof(argv[3], NULL);
  Vg[0] = strtof(argv[4], NULL);
  Vg[1] = strtof(argv[5], NULL);
  Vg[2] = strtof(argv[6], NULL);
  Vm[0] = strtof(argv[7], NULL);
  Vm[1] = strtof(argv[8], NULL);
  Vm[2] = strtof(argv[9], NULL);

  /*
   * one input to set last times to coherent value
   * we toss other data
   */
  nfds = do_epoll_wait (evlist, 1);
  int index = evlist[0].data.fd;
  uint32_t events = evlist[0].events;
  if (events & EPOLLIN) {
    do_input(index, &data);
    lastts[0] = lastts[1] = lastts[2] = data.t;
  } else
    DIE("BUG");

  for (;;) {
    nfds = do_epoll_wait (evlist, MAX_EVENTS);

    for (int n = 0; n < nfds; ++n) {
      int index = evlist[n].data.fd;
      uint32_t events = evlist[n].events;

      if (events & EPOLLIN) {
        do_input (index, &data);
        scal dt = data.t - lastts[index];
        if (dt < 0.0)
          dt = ((float) MAX_SECONDS) - dt;

        lastts[index] = data.t;
        scal vec[3] = {data.x, data.y, data.z};

        switch (index) {
        case ACCEL:
          accelInput(vec, DCM, dt, Va, Pp0);
          break;
        case GYRO:
          gyroInput(vec, DCM, dt, Vg, Pp0, Pp1);
          break;
        case MAGN:
          magnInput(vec, DCM, dt, Vm, Pp1);
	  write(1, DCM, sizeof(DCM));
	  fflush(stdout);
          break;
        }
      } else
        DIE("BUG");
    }
  }
  
  closeSensors();

  exit(EXIT_SUCCESS);
}
